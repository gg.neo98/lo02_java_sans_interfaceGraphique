package noyau;

/**
 * 
 * 
 * Classe héritant de la classe joueur définissant les joueurs artificiels
 * 
 *  
 */
public class JoueurHumain extends Joueur {

	/**
	 * la methode montrerCarte renoit le numéro de carte à donner, vérifie que
	 * celui-ci appartient au résultat désiré et renvoie la carte ayant le numéro
	 * demander
	 * 
	 * @param numeroDeCarte
	 *          
	 * @return carte à montrer
	 */
	public Carte montrerCarte(int numeroDeCarte) {
		if (numeroDeCarte >= 0 && numeroDeCarte < super.getNombreDeCarte()) {
			return super.cartesEnMain.get(numeroDeCarte);
		} else {
			return null;
		}
	}
}
