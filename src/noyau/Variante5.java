package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante5 extends Variante1
{
	public Variante5(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}


	@Override
	public int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		if (((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur()
			)||((Controleur.getNbJoueurs()==2)&& 
			pile.getDerniereCarteJouee().getNomValeur()!="dix"))
		{
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}
		else
		{
		
			return 0;
		}
	}
	

	@Override 
	public int dix (Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		if (((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur()
			)||((Controleur.getNbJoueurs()==2)&& 
			pile.getDerniereCarteJouee().getNomValeur()!="sept"))
		{
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}
		else
		{
			return 0;
		}
	}
	
	@Override
	public int deux(Joueur joueur ,Joueur suivant,Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur()))
			||((pile.getDerniereCarteJouee().getNomValeur()!="sept")
			||pile.getDerniereCarteJouee().getNomValeur()!="dix"))
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
			{
				if (Controleur.getNbJoueurs()==2)
					{return 0;}
				else 
					{return 2;}
			}
	
	}
	
	@Override
	public int as(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	 	{
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
				&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
	 			{
					throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
	 			}
	 		else
	 			{	
	 				int i=1;
	 				Carte carte =null;
	 				//tour
	 				if (suivant instanceof JoueurHumain)
 					{
 							suivant.parcourirMain();
 							int numeroDeCarte = affichage.choixCarte(suivant);
 							if(numeroDeCarte>-1)
 							{
 								carte=suivant.getCarte(numeroDeCarte);
 								suivant.jouerCarte(numeroDeCarte, pile);
 							} 
 								
 					 }
 					else 
 					{
 						JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
 						carte = IA1.strat.executer(IA1, pile, pioche);
 						if (carte!=null)
 						{
 							suivant.jouerCarte(carte, pile);
 						}
 					}
	 				if (carte!=null)
	 				{
	 					if(carte.getNomValeur()=="huit")
	 				
	 					{
	 						return 2;
	 					}
	 					else if (carte.getNomValeur()=="as")
	 					{
	 						return 2;
	 					}
	 				}
	 				
	 					pioche.piocher(2,suivant, pile);
	 					return 1;
	 				
	 			}
	 	}
}
