package noyau;
/**
 * Interface stratégie
 * 
 *
 */
public interface Strategie {
	
	/**
	 * Application de la stratégie au joueur passé en paramètre
	 * @param joueur
	 * @param pile
	 * @param pioche
	 * @return
	 */
	public Carte executer(Joueur j, Pile pile, Pioche pioche);
}
