package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante17 extends Variante1 {
	public Variante17(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int applicationRegle(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		String valCarte = carteJouer.getNomValeur();
		String coulCarte = carteJouer.getNomCouleur();
		int i = 0;
		if (valCarte == "as" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = as(joueur, suivant, carteJouer, pile, pioche);
		}

		if (valCarte == "roi") {
			i = roi(joueur, carteJouer, pile, pioche);
		}

		if (valCarte == "dame" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = dame(joueur, carteJouer, pile, pioche);
		}

		if (valCarte == "valet" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = valet(joueur, suivant, carteJouer, pile, pioche);
		}

		if (valCarte == "dix" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = dix(joueur, carteJouer, pile, pioche);
		}

		if (valCarte == "neuf" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = neuf(joueur, suivant, carteJouer, pile, pioche);
		}

		if (valCarte == "huit" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = huit(joueur, suivant, carteJouer, pile, pioche);
		}

		if (valCarte == "sept" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = sept(joueur, suivant, carteJouer, pile, pioche);
		}

		if (valCarte == "six" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = six(joueur, suivant, carteJouer, pile, pioche);
		}

		if (valCarte == "cinq" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = cinq(joueur, carteJouer, pile, pioche, null);
		}

		if (valCarte == "quatre" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = quatre(joueur, carteJouer, pile, pioche, null);
		}

		if (valCarte == "trois" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = trois(joueur, carteJouer, pile, pioche);
		}

		if (valCarte == "deux" && coulCarte != "jocker1" && coulCarte != "jocker2") {
			i = deux(joueur, suivant, carteJouer, pile, pioche);
		}

		if (coulCarte == "jocker1" || coulCarte == "jocker2") {
			i = joker(joueur, suivant, carteJouer, pile, pioche);
		}

		return i;
	}

	public int as2(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if (pile.getDerniereCarteJouee().getNomValeur() != "as") {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			int i = 1;
			Carte carte = null;
			// tour
			if (suivant instanceof JoueurHumain) {
				suivant.parcourirMain();
				int numeroDeCarte = affichage.choixCarte(suivant);
				if (numeroDeCarte > -1) {
					carte = suivant.getCarte(numeroDeCarte);
					if (carte.getNomValeur() == "as") {
						suivant.jouerCarte(numeroDeCarte, pile);
					}
				}
			} else {
				JoueurArtificiel IA1 = (JoueurArtificiel) suivant;
				carte = IA1.strat.executer(IA1, pile, pioche);
				if (carte != null && carte.getNomValeur() == "as") {
					suivant.jouerCarte(carte, pile);
				}
			}
			if (carte.getNomValeur() == "as") {
				i = i + this.as2(suivant, Controleur.getJoueurSuivant(suivant), carteJouer, pile, pioche);
				return i;
			} else {
				pioche.piocher(2 * i, suivant, null);
				return i;
			}
		}
	}

	@Override
	public int as(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {

			if (joueur.getNombreDeCarte() == 0) {
				pioche.piocher(2, suivant, null);
				return 1;
			} else {
				int i = 1;
				Carte carte = null;
				if (suivant instanceof JoueurHumain) {
					suivant.parcourirMain();
					int numeroDeCarte = affichage.choixCarte(suivant);
					if (numeroDeCarte > -1 && numeroDeCarte < suivant.getNombreDeCarte()) {
						carte = suivant.getCarte(numeroDeCarte);
						if (carte != null && carte.getNomValeur() == "as") {
							suivant.jouerCarte(numeroDeCarte, pile);
						}
					}
				} else {
					JoueurArtificiel IA1 = (JoueurArtificiel) suivant;
					carte = IA1.strat.executer(IA1, pile, pioche);
					if (carte != null && carte.getNomValeur() == "as") {
						suivant.jouerCarte(carte, pile);
					}
				}

				if (carte.getNomValeur() == "as") {
					i = i + this.as2(suivant, Controleur.getJoueurSuivant(suivant), carteJouer, pile, pioche);
					return i;
				} else {
					pioche.piocher(2 * i, suivant, null);
					return i;
				}
			}
		}
	}

	@Override
	// voir comment faire en fonction de comment fonctionne un tour
	public int quatre(Joueur joueur, Carte carteJouer, Pile pile, Pioche pioche, Joueur suivant)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			Carte carte = null;
			do {
				if (suivant instanceof JoueurHumain) {
					suivant.parcourirMain();
					int numeroDeCarte = affichage.choixCarte(suivant);
					if (numeroDeCarte > -1 && numeroDeCarte < suivant.getNombreDeCarte()) {
						carte = suivant.getCarte(numeroDeCarte);
					}
				} else {
					JoueurArtificiel IA1 = (JoueurArtificiel) suivant;
					carte = IA1.strat.executer(IA1, pile, pioche);
					if (carte.equals(carteJouer)) {
						suivant.jouerCarte(carte, pile);
					}
				}
				if (!carte.equals(carteJouer)) {
					pioche.piocher(1, suivant, null);
				}
			} while (!carte.equals(carteJouer));
			return 2;
		}
	}

	@Override
	public int cinq(Joueur joueur, Carte carteJouer, Pile pile, Pioche pioche, Joueur suivant)
			throws PoserCarteException {
		if (joueur.getNombreDeCarte() == 0) {
			pioche.piocher(1, joueur, null);
		}
		return 1;
	}

	@Override
	// probleme des cartes qui se cumul il faut voir comment résoudre ce problème
	// avec les autre méthode
	public int sept(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			return 2;
		}
	}

	@Override
	// problème des couleurs différentes
	public int huit(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if (joueur.getNombreDeCarte() == 0) {
			pioche.piocher(1, joueur, null);
		}
		return 1;
	}

	@Override
	public int neuf(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			int choix;
			int indice;
			do {
				choix = affichage.notifUtilisateuretDonnerInformation(
						"entrer le num�ro du joueur a qui vous vous faire prendre les cartes");
			} while (choix < 0 && choix >= Controleur.getNbJoueurs());
			do {
				indice = affichage.notifUtilisateuretDonnerInformation(
						"entrer le numero de la carte que le joueurs doit prendre");
			} while (indice < 0 && indice >= Controleur.getJoueur(choix).getNombreDeCarte());
			suivant.prendreCarte(Controleur.getJoueur(choix).donnerCarte(indice));
			return 1;
		}
	}

	@Override
	public int dame(Joueur joueur, Carte carteJouer, Pile pile, Pioche pioche) throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			if (joueur.getNombreDeCarte() == 0) {
				pioche.piocher(1, joueur, null);
				return 1;
			} else {
				return 0;
			}
		}
	}

	public int joker(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if (pile.getDerniereCarteJouee().getValeur() == carteJouer.getValeur()
				|| pile.getDerniereCarteJouee().getNomCouleur() == "huit") {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}

		else {
			if (Controleur.getSensJeu() < 0) {
				Controleur.changerSensJeux();
			}
			return 1;
		}
	}

	@Override
	public int valet(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			if (carteJouer.getNomCouleur() == "pique") {
				pioche.piocher(5, suivant, null);
				Controleur.changerSensJeux();
			}
			if (carteJouer.getNomCouleur() == "trefle") {
				pioche.piocher(5, suivant, null);
				Carte carte = null;
				if (suivant instanceof JoueurHumain) {
					suivant.parcourirMain();

					int numeroDeCarte = affichage.choixCarte(suivant);
					if (numeroDeCarte > -1 && numeroDeCarte < suivant.getNombreDeCarte()) {
						carte = suivant.getCarte(numeroDeCarte);
					}
				} else {
					JoueurArtificiel IA1 = (JoueurArtificiel) suivant;
					carte = IA1.strat.executer(IA1, pile, pioche);
					if (carte.equals(carteJouer)) {
						suivant.jouerCarte(carte, pile);
					}
				}
				if (carte.getNomCouleur() == "pique" && carte.getNomValeur() == "valet") {
					suivant.jouerCarte(carte, pile);
				} else {
					pioche.piocher(5, suivant, null);
					Controleur.changerSensJeux();
				}
			}
			return 2;
		}
	}

}
