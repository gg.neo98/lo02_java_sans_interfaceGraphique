package noyau;

import console.Affichable;
/**
 *@see Variante1
 */
public class Variante8 extends Variante1
{
	public Variante8(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
  	public int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
			{return 0;}
	}
	
	public int neuf(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche,Controleur control)throws PoserCarteException
  	{	
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
				&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur()
				&& pile.getDerniereCarteJouee().getNomValeur()!="huit"
				&& pile.getDerniereCarteJouee().getNomValeur()!="joker")
				{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
			else
			{
				if (Controleur.getNbJoueurs()==2)
					{return 0;}
				else 
				{
					Controleur.changerSensJeux();
					return 1;
				}
			}
	}
	
	@Override
	public int dix(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			if (Controleur.getNbJoueurs()==2)
				{return 0;}	
			else
			{
				Controleur.changerSensJeux();
				return 1;
			}
		}
	}
}
