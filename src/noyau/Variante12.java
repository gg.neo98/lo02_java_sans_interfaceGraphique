package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante12 extends Variante11 
{

	public Variante12(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int applicationRegle(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	{
		String valCarte=carteJouer.getNomValeur();
		String coulCarte =carteJouer.getNomCouleur();
		int i=0;
		if (valCarte =="as"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= as(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="roi")
			{ i= roi(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="dame"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= dame(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="valet"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= valet(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="dix"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= dix(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="neuf"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= neuf(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="huit"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= huit(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="sept"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= sept(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="six"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= six(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="cinq"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= cinq(joueur,carteJouer,pile,pioche, null);}
		
		if (valCarte =="quatre"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= quatre(joueur,carteJouer,pile,pioche, null);}
		
		if (valCarte =="trois"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= trois(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="deux"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= deux(joueur,suivant,carteJouer,pile,pioche);}
		
		if (coulCarte =="jocker1" || coulCarte =="jocker2")
			{i=joker(joueur ,suivant,carteJouer ,pile,pioche);}
		
		return i;
	}
	
	@Override
  	public int valet(Joueur joueur , Joueur suivant , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
  	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
  			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
  		else
  			{return 1;}
  	}
	
	@Override
	public int sept(Joueur joueur ,  Joueur suivant ,Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
  	{	
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
				&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
				{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
			else
			{
				if (Controleur.getNbJoueurs()==2)
					{return 0;}
				else 
					{
						Controleur.changerSensJeux();
						return 1;
					}
			}
	}
	
	
	public int joker(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		pioche.piocher(4,suivant, pile);
		int couleurJocker;
		couleurJocker=affichage.choixCouleur();
		carteJouer.changerCouleur(couleurJocker);
		return 1;
	}
}
