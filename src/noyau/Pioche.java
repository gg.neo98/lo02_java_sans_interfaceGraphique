package noyau;

import java.util.Collections;

/**Pioche est un obet qui va stocker les cartes de la pioche et va ainsi permettre leur acc�s 
 * @version 2.0
 * @author guillaume
 *
 */

import java.util.LinkedList;

import console.Affichable;
import console.Affichage;
/**
 * Classe définissant l'objet de type pioche, soit l'ensemble des cartes piochables sous forme d'une linkedlist
 * 
 *
 */
public class Pioche {

	private LinkedList<Carte> tabPioche;
	private int nbCartesPioche;
	private Affichable affichage;


	/**
	 * Constructeur de la pioche
	 * @param une linkedlist de carte disponibles après la création des mains
	 * @param une objet affichable pour l'affichage
	 * @see JeuDeCarte          
	 */
	public Pioche(LinkedList<Carte> tab,Affichable a)
	{
		this.affichage=a;
		this.tabPioche = tab;
		this.nbCartesPioche=this.tabPioche.size();
	}

	/**
	 * Permet d'obtenir la première carte de la pioche
	 * @return la première carte
	 */
 
	public Carte getPremiereCarte() {
		this.nbCartesPioche--;
		return this.tabPioche.removeFirst();
	}



	

	public int getNbCartesPioche() {
		return nbCartesPioche;
	}




	public void setNbCartesPioche(int nbCartesPioche) {
		this.nbCartesPioche = nbCartesPioche;
	}
	/**
	 * Permet de piocher
	 * @param nbCartesAPiocher
	 * @param preneur
	 * @param la pile pour récréer la pioche si celle ci est vide
	 */
	public void piocher(int nbCartesAPiocher, Joueur preneur, Pile p) {
		int i;
		for (i = 0; i < nbCartesAPiocher; i++) 
		{
			try
			{
				if (this.tabPioche.size()==0) 
				{
					this .genererPioche(p);
				}
				preneur.prendreCarte(getPremiereCarte());
			}
			catch(PiocheEtPileVideExeption e)
			{
				affichage.notifUtilisateur("il n'y a plus de carte vous ne pouver plus piocher");
			}
		}
	}
	/**
	 * Permet de recréer la pioche
	 * Lance une exception quand la pile est vide, car on ne peut plus l'utiliser pour recréer la pioche
	 * @param pile
	 * @throws PiocheEtPileVideExeption
	 */
	private void genererPioche(Pile pile) throws PiocheEtPileVideExeption{

		LinkedList<Carte> tabPioche;
		tabPioche = pile.getTabCartesPile();
		if (tabPioche.size()!=0)
		{
			Collections.shuffle(tabPioche);
			this.setTabPioche(tabPioche);
			LinkedList<Carte> newPile = new LinkedList <Carte>();
			newPile.add(this.getPremiereCarte());
			pile.setTabCartesPile(newPile);
		}
		else 
		{
			throw new PiocheEtPileVideExeption() ; 
		}
	}

	/**
	 * Permet de donner un nombre de carte à un autre joueur
	 * *
	 * 
	 * @param le joueur à qui on donne les cartes
	 * @param nombreDeCarteADonner
	 *           
	 */
	public void donnerCarte(Joueur personne, int nombreDeCarteADonner) {
	
		for (int i = 0; i < nombreDeCarteADonner; i++) {
			personne.prendreCarte(this.tabPioche.remove());
			this.nbCartesPioche--;
		}
	}

	public LinkedList<Carte> getTabPioche() {
		return tabPioche;
	}

	public void setTabPioche(LinkedList<Carte> tabPioche) {
		this.tabPioche = tabPioche;
	}
}
