package noyau;

import console.Affichable;
/**
 * 
 * Exception gérant la situation où la carte sélectionnée ne peut pas être jouée
 * @author day
 *
 */
public class PoserCarteException extends Exception

{
	/**
	 * Notifie le joueur que la carte n'est pas jouable
	 * @param joueur
	 * @param carteJouée
	 * @param derniereCarteJouee
	 * @param affichage
	 */
	public PoserCarteException(Joueur j, Carte carteJouer, Carte derniereCarteJouee,Affichable affichage)
	{
		affichage.notifUtilisateur("Impossible de jouer cette carte !\n"+"dernière carte de la pile : "+derniereCarteJouee.toString()+"\n"+"carte selectionnee"+carteJouer.toString()+"\n");
		
	}
}
