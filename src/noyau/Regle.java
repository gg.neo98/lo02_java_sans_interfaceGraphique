package noyau;

import console.Affichable;
/**
 * 
 * Classe abstraite contenant les methodes permettant d'appliquer à la carte jouée, les règles de la variante utiliée pour la partie en cours
 * La classe est abstraite car on redéfinie les méthdes pour chaque variante
 * @see Variante1
 */
public abstract class Regle {
	protected Affichable affichage;
	
	public Regle(Affichable a){
		this.affichage=a;
		}
	
	public static int nombreCarte;
	
	/**
	 * Permet d'appeler la methode correspondant à la carte à jouer (en fonction de sa valeur)
	 * 
	 * @param joueur
	 * @param suivant
	 * @param carteJouer
	 * @param pile
	 * @param pioche
	 * @return
	 * @throws PoserCarteException
	 */
	public int applicationRegle(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	{
		String valCarte=carteJouer.getNomValeur();
		int i=0;
		if (valCarte =="as")
		{ i= as(joueur,suivant,carteJouer,pile,pioche);}
		if (valCarte =="roi")
		{ i= roi(joueur,carteJouer,pile,pioche);}
		if (valCarte =="dame")
		{ i= dame(joueur,carteJouer,pile,pioche);}
		if (valCarte =="valet")
		{ i= valet(joueur,suivant,carteJouer,pile,pioche);}
		if (valCarte =="dix")
		{ i= dix(joueur,carteJouer,pile,pioche);}
		if (valCarte =="neuf")
		{ i= neuf(joueur,suivant,carteJouer,pile,pioche);}
		if (valCarte =="huit")
		{ i= huit(joueur,suivant,carteJouer,pile,pioche);}
		if (valCarte =="sept")
		{ i= sept(joueur,suivant,carteJouer,pile,pioche);}
		if (valCarte =="six")
		{ i= six(joueur,suivant,carteJouer,pile,pioche);}
		if (valCarte =="cinq")
		{ i= cinq(joueur,carteJouer,pile,pioche, null);}
		if (valCarte =="quatre")
		{ i= quatre(joueur,carteJouer,pile,pioche, null);}
		if (valCarte =="trois")
		{ i= trois(joueur,carteJouer,pile,pioche);}
		if (valCarte =="deux")
		{ i= deux(joueur,suivant,carteJouer,pile,pioche);}
		return i;
	}
	public abstract int as(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche)throws PoserCarteException;

	public abstract int roi(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int dame(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int valet(Joueur joueur , Joueur suivant , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int dix(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int neuf(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int huit(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int six(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche piocher) throws PoserCarteException;

	public abstract int cinq(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche, Joueur suivant)throws PoserCarteException;

	public abstract int quatre(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche, Joueur suivant)throws PoserCarteException;

	public abstract int trois(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;

	public abstract int deux(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException;
}
