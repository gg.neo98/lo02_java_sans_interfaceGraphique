package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante11 extends Variante1
{
	public Variante11(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int dix(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException
  	{	
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
				{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
			else
			{
					return 0;
			}
	}
	
	@Override
  	public int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{	
			if(Controleur.getNbJoueurs()==2)
				{return 0;}
			else
				{return 2;}
		}
	}
	
	@Override
	public int valet(Joueur joueur , Joueur suivant , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
  	{ 	
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{PoserCarteException poserCarteException = new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
			throw poserCarteException;}
		else
		{
			if (Controleur.getNbJoueurs()==2)
				{return 0;}
			else 
			{
				Controleur.changerSensJeux();
				return 1;
			}
		}
  	}
	
	@Override
	public int as(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	 {
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}
		else
		{	
			int i=1;
			int numeroDeCarte=-1;
			Carte carte = null;
			//tour
	 		if (suivant instanceof JoueurHumain)
 			{
	 			suivant.parcourirMain();
 				numeroDeCarte = affichage.choixCarte(suivant);
 				if(numeroDeCarte>-1)
 				{
 					carte=suivant.getCarte(numeroDeCarte);
 				} 
 				
 								
 			}
 			else 
 			{
 				JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
 				carte = IA1.strat.executer(IA1, pile, pioche);
 			}
	 		if(carte !=null)
	 		{
	 			if (carte.getNomValeur()=="as")
	 		
	 			{
	 				suivant.jouerCarte(carte, pile);
	 				if (suivant.getNombreDeCarte()==0)
	 					{Controleur.gagner();}
	 				else
	 					{i=i+this.as2(suivant, Controleur.getJoueurSuivant(suivant), carteJouer, pile, pioche);}
	 				return i;
	 			}
	 		}
	 		else
	 		{
	 			pioche.piocher(2*i,suivant, pile);
	 			return i;
	 		}
	 		return 0;
	 	}
	 }
	public int as2(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche ) throws PoserCarteException
	{
		if (pile.getDerniereCarteJouee().getNomValeur()!="huit"
			&&pile.getDerniereCarteJouee().getNomValeur()!="as")
		{
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}
		else
		{	
 			int i=1;
 			Carte carte = null;
 			if (suivant instanceof JoueurHumain)
			{
				suivant.parcourirMain();
				int numeroDeCarte = affichage.choixCarte(suivant);
				if(numeroDeCarte>-1)
				{
					carte=suivant.getCarte(numeroDeCarte);
					//suivant.jouerCarte(numeroDeCarte, pile);
				} 						
			 }
			else 
			{
				JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
				carte = IA1.strat.executer(IA1, pile, pioche);
			}
 			if(carte.getNomValeur()=="huit")
 			{
 				suivant.jouerCarte(carte, pile);
 				if (suivant.getNombreDeCarte()==0)
					{Controleur.gagner();}
 				return 1;
 			}		
 			else if (carte.getNomValeur()=="as")
 			{
 				suivant.jouerCarte(carte, pile);
 				if (suivant.getNombreDeCarte()==0)
					{Controleur.gagner();}
				else
					{i=i+this.as2(suivant, Controleur.getJoueurSuivant(suivant), carteJouer, pile, pioche);}
 				return i;
 			}
 			else
 			{
 				pioche.piocher(2*i,suivant, pile);
				return i;
			}
		}
	}
	
	//pour le huit je ne comprends pas la règle je peux donc pas l'adapter
}
