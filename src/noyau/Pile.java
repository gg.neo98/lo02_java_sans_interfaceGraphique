package noyau;
/**
 * @version 1.0
 * @author guillaume
 *la class pile est l'objet qui va permettre de stocker les différentes cartes jouer
 */

//import java.util.ArrayList;
import java.util.LinkedList;
/**
 * Classe définissant les objets de type pile, soit la pile de cartes jouées sous forme d'une linkedlist
 * 
 *
 */
public class Pile {

	private int nbCartesPile;
	private LinkedList<Carte> tabCartesPile;
	private Carte derniereCarte;

	/**
	 * Constucteur de la class Pile
	 * On prend la pioche en paramètre car on prend la dernière carte de la pioche pour créer la pile
	 * @param la pioche
	 * 
	 */
	public Pile(Pioche pioche) {
		this.nbCartesPile = 0;
		this.derniereCarte = pioche.getPremiereCarte();
		this.tabCartesPile= new LinkedList<Carte>();
		this.tabCartesPile.add(this.getDerniereCarteJouee());
	}
	/**
	 * Permet de poser une carte sur la pile
	 * @see Joueur#jouerCarte(Carte, Pile)
	 * @param carte à poser
	 */
	public void poserCarte(Carte c) {
		this.derniereCarte = c;
		this.tabCartesPile.add(c);
		this.nbCartesPile++;
	}
	
	 
	 
	 
	public int getNombreCarte() {
		return this.nbCartesPile;
	}

	public Carte setCarte(int numeroCarte) {

		return this.tabCartesPile.remove(numeroCarte);
	}

	public Carte getDerniereCarteJouee() {
		return this.derniereCarte;
	}

	public void setTabCartesPile(LinkedList<Carte> tabCartesPile) {
		this.tabCartesPile = tabCartesPile;
	}

	public LinkedList<Carte> getTabCartesPile() {
		return tabCartesPile;
	}
	
	public void enleverDerniereCarteJouer()
	{
	
		this.tabCartesPile.remove(this.derniereCarte);
		this.derniereCarte=this.tabCartesPile.getLast();
	}


}