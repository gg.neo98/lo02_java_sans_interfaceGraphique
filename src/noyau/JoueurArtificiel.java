package noyau;

/**
 * 
 * Classe héritant de la classe joueur définissant les joueurs artificiels
 * Permet la grstion des stratégies
 *@see Joueur
 */
public class JoueurArtificiel extends Joueur {

	Strategie strat;
	/**
	 * COnstructeur de la classe
	 * Attribue une stratégie au joueur artificiel
	 * @param stratégie
	 */
	public JoueurArtificiel(Strategie s) {
		this.strat=s;
		
	}
	/**
	 * Permet d'attribuer une stratégie à un joueur artificiel
	 * @param stratégie
	 */
	public void setStrat(Strategie s) {
		this.strat=s;
	}
}
