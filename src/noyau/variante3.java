package noyau;

import java.util.ArrayList;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class variante3 extends variante2
{
	public variante3(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
  	public int roi(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur()
			&&pile.getDerniereCarteJouee().getNomValeur()!="dame")
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
	else
		{return 1;}
	}
	
	@Override 
	public int dame(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
				&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur()
				&&pile.getDerniereCarteJouee().getNomValeur()!="roi")
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
			{return 1;}
	}
	
	@Override
	public int six(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
			{
				pioche.piocher(1,suivant, null);
				return 1;
			}
	}
	
	@Override
	public int neuf(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
			{
				if (joueur.getNombreDeCarte()>1)
				{
					int indice;
					if (suivant instanceof JoueurHumain)
					{
						do
							{indice=affichage.notifUtilisateuretDonnerInformation("veiller choisir la carte que vous voulez prendre");}
						while (indice<0||indice>=joueur.getNombreDeCarte());
					}
					else 
					{
						if (joueur.getindexcarte(carteJouer)!=0)
							{indice =0;}
						else 
							{indice=1;}
					}
					suivant.prendreCarte(joueur.donnerCarte(indice));
				}
				return 1;
			}
	}
	
	@Override
	public int as(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
			{
				int indice;
				Joueur temp;
				//affichage demander le numero du joueur a qui ont veut donner 2 De ses cartes 
				//affichage pour demander quel carte le joueur veut il prendre
				if (joueur instanceof JoueurHumain)
				{
					do
					{
						indice=affichage.notifUtilisateuretDonnerInformation("entrer le numero du joueur dont vous voulez donner 2 de vos carte");
					}while(indice<0||indice>=Controleur.getNbJoueurs());
					temp=Controleur.getJoueur(indice);
					if (temp.getNombreDeCarte()>1)
						{
						do
							{indice=affichage.notifUtilisateuretDonnerInformation("quel carte voulez vous prendre");}
						while (indice<0||indice>=temp.getNombreDeCarte());	
						suivant.prendreCarte(temp.donnerCarte(indice));}
					if (temp.getNombreDeCarte()>1)
					{
						do
							{indice=affichage.notifUtilisateuretDonnerInformation("quel carte voulez vous prendre");}
						while (indice<0||indice>=temp.getNombreDeCarte()||indice!=joueur.getindexcarte(carteJouer));	
						suivant.prendreCarte(temp.donnerCarte(indice));
					}
				}
				else
				{
					temp=Controleur.getJoueur((int)Math.random()*(Controleur.getNbJoueurs()-1));
					if (temp.getNombreDeCarte()>1)
					{	
						suivant.prendreCarte(temp.donnerCarte(0));
						suivant.prendreCarte(temp.donnerCarte(1));
					}
				}
				if (temp.getNombreDeCarte()==0)
				{
					Controleur.gagner();
				}
				return 1;
			}
	}
	
	@Override
	public int quatre(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche, Joueur suivant) throws PoserCarteException
  	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
  			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
  		else
  			{
  			
  			int indice = 0;
  			if (joueur  instanceof JoueurHumain)
  			{
  				indice=affichage.notifUtilisateuretDonnerInformation(" ");
  			
  			}
  				
  			if(((1+(int)Math.random()*(5)%2)==0)||(indice!=4))
 			{
 				pioche.piocher(1,joueur, pile);
 			}
  			
  			return 1;
  			}
  	}
	
	@Override
	public int cinq(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche, Joueur suivant) throws PoserCarteException
	{ 	
		return 1;
	}
	
	@Override
 	public int trois(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			if (carteJouer.getNomCouleur()=="coeur")
			{
				Joueur joueurtemp ;
				joueurtemp = Controleur.getJoueurPrecedent(joueur);
				ArrayList <Carte> jeux=joueurtemp.getJeuxEnMain();
				joueurtemp.setJeuxEnMain(joueur.getJeuxEnMain());
				joueur.setJeuxEnMain(jeux);
				return 1;
			}
			else
				{return 1;}
		}
	}
	
	
}
