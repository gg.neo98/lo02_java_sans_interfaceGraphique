package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante13 extends Variante1 {
	
	public Variante13(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int applicationRegle(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche ) throws PoserCarteException
	{
		String valCarte=carteJouer.getNomValeur();
		String coulCarte =carteJouer.getNomCouleur();
		int i=0;
		if (valCarte =="as"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= as(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="roi")
			{ i= roi(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="dame"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= dame(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="valet"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= valet(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="dix"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= dix(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="neuf"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= neuf(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="huit"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= huit(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="sept"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= sept(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="six"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= six(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="cinq"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= cinq(joueur,carteJouer,pile,pioche, null);}
		
		if (valCarte =="quatre"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= quatre(joueur,carteJouer,pile,pioche, null);}
		
		if (valCarte =="trois"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= trois(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="deux"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= deux(joueur,suivant,carteJouer,pile,pioche);}
		
		if (coulCarte =="jocker1" || coulCarte =="jocker2")
			{i=joker(joueur ,suivant,carteJouer ,pile,pioche);}
		
		return i;
	}
	
	@Override
	public int dame(Joueur joueur, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()
				&& pile.getDerniereCarteJouee().getNomValeur() != "roi") {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			return 1;
		}
	}

	@Override
	public int roi(Joueur joueur, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			if (Controleur.getNbJoueurs() == 2) {
				return 0;
			} else {
				Controleur.changerSensJeux();
				return 1;
			}
		}
	}

	@Override
	public int valet(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		if ((pile.getDerniereCarteJouee().getNomValeur() != carteJouer.getNomValeur())
				&& (pile.getDerniereCarteJouee().getNomCouleur()) != carteJouer.getNomCouleur()) {
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		} else {
			return 2;
		}
	}

	@Override
	public int as(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	 {
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}
		else
		{	
			int i=1;
			int numeroDeCarte=-1;
			Carte carte = null;
			//tour
	 		if (suivant instanceof JoueurHumain)
 			{
	 			suivant.parcourirMain();
 				numeroDeCarte = affichage.choixCarte(suivant);
 				if(numeroDeCarte>-1)
 				{
 					carte=suivant.getCarte(numeroDeCarte);
 				} 
 				
 								
 			}
 			else 
 			{
 				JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
 				carte = IA1.strat.executer(IA1, pile, pioche);
 			}
	 		if(carte !=null)
	 		{
	 			if (carte.getNomValeur()=="as")
	 		
	 			{
	 				suivant.jouerCarte(carte, pile);
	 				if (suivant.getNombreDeCarte()==0)
	 					{Controleur.gagner();}
	 				else
	 					{i=i+this.as2(suivant, Controleur.getJoueurSuivant(suivant), carteJouer, pile, pioche);}
	 				return i;
	 			}
	 		}
	 		else
	 		{
	 			pioche.piocher(2*i,suivant, pile);
	 			return i;
	 		}
	 		return 0;
	 	}
	 }
	public int as2(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche ) throws PoserCarteException
	{
		if (pile.getDerniereCarteJouee().getNomValeur()!="as")
		{
			throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
		}
		else
		{	
 			int i=1;
 			Carte carte = null;
 			if (suivant instanceof JoueurHumain)
			{
				suivant.parcourirMain();
				int numeroDeCarte = affichage.choixCarte(suivant);
				if(numeroDeCarte>-1)
				{
					carte=suivant.getCarte(numeroDeCarte);
					//suivant.jouerCarte(numeroDeCarte, pile);
				} 						
			 }
			else 
			{
				JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
				carte = IA1.strat.executer(IA1, pile, pioche);
			}
 			if (carte.getNomValeur()=="as")
 			{
 				suivant.jouerCarte(carte, pile);
 				if (suivant.getNombreDeCarte()==0)
					{Controleur.gagner();}
				else
					{i=i+this.as2(suivant, Controleur.getJoueurSuivant(suivant), carteJouer, pile, pioche);}
 				return i;
 			}
 			else
 			{
 				pioche.piocher(2*i,suivant, pile);
				return i;
			}
		}
	}
	public int joker(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		// tour//carte obtenue avec tour
			Carte carte =null;
			//tour
			if (suivant instanceof JoueurHumain)
			{
					suivant.parcourirMain();
					int numeroDeCarte = affichage.choixCarte(suivant);
					if(numeroDeCarte>-1)
					{
						do
							{carte=suivant.getCarte(numeroDeCarte);}
						while (carte!=null&&(carte.getNomValeur() == "jocker1")||(carte.getNomValeur() == "jocker2") );
						suivant.jouerCarte(numeroDeCarte, pile);
					} 
						
			 }
			else 
			{
				JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
				carte = IA1.strat.executer(IA1, pile, pioche);
				if (carte!=null)
				{
					suivant.jouerCarte(carte, pile);
				}
			
			}
		if ((carte.getNomValeur() == "jocker1")||(carte.getNomValeur() == "jocker2") )
		{
			pioche.piocher(6, Controleur.getJoueurSuivant(suivant), pile);
			return 2;
		} 
		else 
		{
			pioche.piocher(3, suivant, pile);
			return 1;
		}

	}

	@Override
	public int huit(Joueur joueur, Joueur suivant, Carte carteJouer, Pile pile, Pioche pioche)
			throws PoserCarteException {
		// demander la couleurs de la carte
		
		int indice=affichage.choixCouleur();
		Carte carte = carteJouer;
		int couleursCarteTemp = carteJouer.getCouleur();
		carteJouer.changerCouleur(indice);
		affichage.afficherCarteJouee(pile.getDerniereCarteJouee());
		if (suivant instanceof JoueurHumain)
		{
			suivant.parcourirMain();
			int numeroDeCarte = affichage.choixCarte(suivant);
			suivant.jouerCarte(numeroDeCarte, pile);
			}
		else
		{ 
			JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
			carte = IA1.strat.executer(IA1, pile, pioche);
		}
		indice= this.applicationRegle(suivant, Controleur.getJoueurSuivant(suivant), pile.getDerniereCarteJouee(), pile, pioche);
		carte.changerCouleur(couleursCarteTemp);
		return indice+1;
	}
	

}
