package noyau;

/**
 * Classe définissant l'objet carte au sein du jeu
 * 
 * 
 */
public class Carte {

	private int valeur;
	private int couleur;
	private Valeur[] tabValeurs;
	private Couleur[] tabCouleurs;

	/**
	 * 
	 * Constructeur permettant de construire un objet de type carte en fonction
	 * d'une couleur et d'un valeur
	 * 
	 * @param couleur
	 * @param valeur
	 */
	public Carte(int couleur, int valeur) {
		this.tabValeurs = Valeur.values();
		this.tabCouleurs = Couleur.values();
		this.valeur = valeur;
		this.couleur = couleur;
	}

	/**
	 * Surcharge du constructeur pour construire une carte uniquement en fonction
	 * d'une couleur
	 * 
	 * @param couleur
	 */
	public Carte(int couleur) {
		this.tabValeurs = Valeur.values();
		this.tabCouleurs = Couleur.values();
		this.couleur = 5;
		this.valeur = couleur;

	}

	/**
	 * Permet d'obtenir la valeur d'une carte
	 * 
	 * @return la valeur sous forme d'entier
	 */
	public int getValeur() {
		return this.valeur;
	}

	/**
	 * Permet d'obtenir le nom de la valeur d'une carte
	 * 
	 * @return la valeur sous forme d'une chaine de caractères
	 */
	public String getNomValeur() {
		return this.tabValeurs[this.valeur].getValeur();
	}

	/**
	 * Permet d'obtenir la couleur d'une carte
	 * 
	 * @return la couleur sous forme d'entier
	 */
	public int getCouleur() {
		return this.couleur;
	}

	/**
	 * Permet d'obtenir le nom de la couleur d'une carte
	 * 
	 * @return la couleur sous forme d'une chaine de caractère
	 */
	public String getNomCouleur() {
		return this.tabCouleurs[this.couleur].getValeur();
	}

	/**
	 * Permet de changer la couleur d'une carte
	 * 
	 * @param l'indice de la couleur
	 *            
	 */
	public void changerCouleur(int indice) {

		this.couleur = indice;

	}

	/**
	 * Permet d'obtenir une chaine de caractères définissant la carte
	 * 
	 * @return la chaine de caractères
	 * 
	 */
	public String toString() {
		return "Couleur :" + getNomCouleur() + "\nValeur :" + getNomValeur();
	}
	
	/**
	 * Permet qu'une carte est jouable
	 * 
	 * @param la carte sur laquelle on joue
	 * @return le résultat du test sous forme d'un booléen
	 */
	public boolean equals(Carte c) {
		if (this.couleur == c.couleur || this.valeur == c.valeur) {
			return true;
		}
		return false;
	}
}
