package noyau;

import console.Affichable;
/**
 *@see Variante1
 */
public class variante2 extends Variante1
{
	public variante2(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
	else
		{return 2;}
	}
	
	@Override
	public  int neuf (Joueur joueur , Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException 
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			pioche.piocher(1,suivant, pile);
			return 1;
		}
	}
	
	@Override
	public int valet(Joueur joueur ,  Joueur suivant ,Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			Controleur.changerSensJeux();
			return 1;
		}
	}
	
	@Override
	public int huit(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		return 1;
	}
	@Override
	//a modifier mais voir si on ne fait pas un methode de tour a un joueur 
	 public int as(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	 	{
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur()
			)&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
	 		{
				throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
	 		}
	 		else
	 			{return 1;}
	 	}
}
