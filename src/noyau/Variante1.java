package noyau;

import console.Affichable;
/**
 * Classe définissant les diffents effets des cartes selon les règles de la variante1
 * 
 *
 */
public class Variante1  extends Regle
{	
	/**
	 * Constructeur de la classe
	 * La variante hérite de règle
	 * @see Regle
	 * @param affichable
	 */
	public Variante1(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	public static final int numeroDeRegle =1;
	/**
	 * Redéfinition de la méthode as de la classe Regle
	 * @see Regle#as(Joueur, Joueur, Carte, Pile, Pioche)
	 * Permet d'appliquer les effet des as en fonction de la variante1
	 * Lance PoserCarteExeption si la carte n'est pas jouable
	 * @param joueur
	 * @param joueursuivant
	 * @param cartejouee
	 * @param pile
	 * @param pioche
	 * @throws PoserCarteException
	 * @return la methode renvoie 1, la variable tour sera incrémentée de 1
	 * @see Controleur
	 */
	@Override
	 public int as(Joueur joueur , Joueur suivant,Carte carteJouee , Pile pile , Pioche pioche) throws PoserCarteException
 	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouee.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouee.getNomCouleur())
 			{
				throw new PoserCarteException(joueur, carteJouee, pile.getDerniereCarteJouee(),this.affichage);
 			}
 		else
 			{return 1;}
 	}
	
	 @Override
  	public int roi(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	 
	@Override
  	public int dame(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	
	@Override
  	public int valet(Joueur joueur , Joueur suivant , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
  	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
  		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
  	}
	
	@Override
  	public int dix(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException
  	{	
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
		else
		{
			if(joueur.getNombreDeCarte()==0)
			{
				pioche.piocher(1, joueur, pile);
				return 1;
			}
			else
			{
				return 0;
			}
		}
   	}
	
	@Override
  	public int neuf(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	
	@Override
  	public int huit(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		return 1;
	}
	
	@Override
  	public int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	
	@Override
  	public int six(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche piocher) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	
	@Override
  	public int cinq(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche, Joueur suivant) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	
	@Override
  	public int quatre(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche, Joueur suivant) throws PoserCarteException
  	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
  		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
  			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
  		else
  			{return 1;}
  	}
	
	@Override
  	public int trois(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
	
	@Override
  	public int deux(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
		&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(),this.affichage);}
	else
		{return 1;}
	}
}