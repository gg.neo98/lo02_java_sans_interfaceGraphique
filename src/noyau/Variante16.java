package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante16 extends Variante1
{
	public Variante16(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int applicationRegle(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	{
		String valCarte=carteJouer.getNomValeur();
		String coulCarte =carteJouer.getNomCouleur();
		int i=0;
		if (valCarte =="as"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= as(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="roi")
			{ i= roi(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="dame"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= dame(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="valet"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= valet(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="dix"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= dix(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="neuf"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= neuf(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="huit"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= huit(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="sept"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= sept(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="six"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= six(joueur,suivant,carteJouer,pile,pioche);}
		
		if (valCarte =="cinq"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= cinq(joueur,carteJouer,pile,pioche, null);}
		
		if (valCarte =="quatre"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= quatre(joueur,carteJouer,pile,pioche, null);}
		
		if (valCarte =="trois"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= trois(joueur,carteJouer,pile,pioche);}
		
		if (valCarte =="deux"&& coulCarte !="jocker1" && coulCarte !="jocker2")
			{ i= deux(joueur,suivant,carteJouer,pile,pioche);}
		
		if (coulCarte =="jocker1" || coulCarte =="jocker2")
			{i=joker(joueur ,suivant,carteJouer ,pile,pioche);}
		
		return i;
	}
	
	@Override
	public int deux(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			int nbCarteDonner=0;
			int carte;
			int choix;
			while (nbCarteDonner<3)
				{
					do
					{
						//demander joueur qui doit prendre et le nombre de carte a prendre
						carte = affichage.notifUtilisateuretDonnerInformation("combien de carte voulez vous donnez au maximum 3");
						choix= affichage.notifUtilisateuretDonnerInformation("a qui voulez vous Donner ces cartes");
					}	
					while(carte<=0&&carte>3-nbCarteDonner&& choix>=0&&choix<Controleur.getNbJoueurs());
					pioche.piocher(carte,Controleur.getJoueur(choix), pile);
				}
		}
		return 1;
	}
	

	@Override
  	public int valet(Joueur joueur , Joueur suivant , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
  	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
	else
		{return 2;}
  	}
	
	@Override
  	public int huit(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		return 1;//probleme du changement de figure
	}
	
	
	@Override
	public int dix(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException
  	{	
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
				&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
				{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
			else
			{
				if (Controleur.getNbJoueurs()==2)
					{return 0;}
				else 
					{
						Controleur.changerSensJeux();
						return 1;
					}
			}
	}
	
	@Override
	 public int as(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche) throws PoserCarteException
	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
				&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{
				throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
			}
		else
			{
				int choix;
				do{
				choix = affichage.notifUtilisateuretDonnerInformation("entrer le num�ro du joueur a qui vous vous faire prendre les cartes");
				}
				while(choix<0||choix>Controleur.getNbJoueurs());
				pioche.piocher(2,Controleur.getJoueur(choix), pile);
				return 1;
			}
	}
	
	public int joker(Joueur joueur ,Joueur suivant, Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{
		int nbCarteDonner=0;
		int carte;
		int choix;
		while (nbCarteDonner<6)
			{
				do
				{
					carte = affichage.notifUtilisateuretDonnerInformation("combien de carte voulez vous donnez au maximum 6");
					choix= affichage.notifUtilisateuretDonnerInformation("a qui voulez vous Donner ces cartes");
				}					
				while(carte<=0&&carte>6-nbCarteDonner&& choix>=0&&choix<Controleur.getNbJoueurs());
				pioche.piocher(carte,Controleur.getJoueur(choix), pile);
			}
		return 1;
	}
}
