package noyau;

import java.util.ArrayList;
import java.util.ListIterator;
/**
 * Stratégie aggressive
 * le joueur artificiel jouera des cartes ayant des effets aggressifs (faire piocher/sauter le tout de l'adversaire) quand c'est possible
 * sinon il joue normalement
 * 
 *
 */
public class StrategieAggressive implements Strategie {

	/**
	 * Application de la stratégie au joueur artificiel passé en paramètre
	 * @param joueur
	 * @param pile
	 * @param pioche
	 */
	@Override
	public Carte executer(Joueur j, Pile pile, Pioche pioche) {
		Carte c = pile.getDerniereCarteJouee();
		Regle r = Controleur.getRegle();
		ArrayList<Carte> main = j.getCartesEnMain();
		ListIterator<Carte> i = main.listIterator();

		if (r instanceof variante2) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "neuf" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator(); // on recrée un iterator pour reparcourir la main
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {
					return c2;
				}
			}
			return null;
		}
		if (r instanceof variante3) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "six" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "sept" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator(); // on recrée un iterator pour reparcourir la main
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante4) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "sept" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "huit" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante5) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "deux" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante6) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "valet" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "deux" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "joker") {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante7) {
			while (i.hasNext()) {
				Carte c2 = i.next();

				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante8) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "neuf" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "joker") {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante9) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "sept" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "dame" && c2.getNomCouleur() == "trefle" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante10) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "dame" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante11) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "sept" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "deux" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante12) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "six" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "joker") {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante13) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "valet" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "joker") {
					return c2;
				}
				if (c2.getNomValeur() == "huit") {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante14) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "sept" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "valet" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		}
		if (r instanceof Variante15) {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.getNomValeur() == "valet" && c2.getNomCouleur() == "pique" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "dame" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "as" && c2.equals(c)) {
					return c2;
				}
				if (c2.getNomValeur() == "sept" && c2.equals(c)) {
					return c2;
				}

			}
			ListIterator<Carte> k = main.listIterator();
			while (k.hasNext()) {
				Carte c2 = k.next();
				if (c2.equals(c)) {

					return c2;
				}
			}
			return null;
		} else {
			while (i.hasNext()) {
				Carte c2 = i.next();
				if (c2.equals(c)) {

					return c2;
				}
			}

			return null;
		}

	}
}
