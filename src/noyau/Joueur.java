package noyau;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * Classe représentant un joueur. Le joueur possède une main qu'il peut
 * parcourir piocher et jouer des cartes
 * 
 */
public class Joueur {
	;
	protected int nbCartes;
	
	protected ArrayList<Carte> cartesEnMain;
	private ListIterator<Carte> i;

	/**
	 * COnstructeur de la classe
	 */
	public Joueur() {
		this.cartesEnMain = new ArrayList<>();
		i = cartesEnMain.listIterator();

	}

	/**
	 * Permet de prendre une carte et de l'ajouter à sa main
	 * 
	 * @param cartePrise
	 */
	public void prendreCarte(Carte cartePrise) {
		this.nbCartes++;
		this.cartesEnMain.add(cartePrise);
	}
	/**
	 * Permet de jouer une carte
	 * @param l'indice de la carte dans la main du joueur
	 * @param la pile dela partie en cours
	 */
	public void jouerCarte(int numeroDeCarte, Pile p) {

		Carte carteJouee = this.cartesEnMain.get(numeroDeCarte);
		p.poserCarte(carteJouee);
		this.cartesEnMain.remove(numeroDeCarte);
	}
	/**
	 * Surcharge de la methode jouerCarte. 
	 * @param la carte à jouer
	 * @param la pile de la partie
	 */
	public void jouerCarte(Carte carte, Pile p) {

		int numeroDeCarte = this.cartesEnMain.indexOf(carte);
		Carte carteJouee = this.cartesEnMain.get(numeroDeCarte);
		p.poserCarte(carteJouee);
		this.cartesEnMain.remove(numeroDeCarte);

	}
	/**
	 * Permet au joueur de parcouvrir sa main
	 */
	public void parcourirMain() {
		
		for (int i = 0; i < this.getCartesEnMain().size(); i++) {
			System.out.println("Carte #" + i);
			System.out.println(this.cartesEnMain.get(i).toString());
		}

	}
	/**
	 * Permet d'obtenir une carte dans la main
	 * @param indice de la carte
	 * @return la carte
	 */
	public Carte getCarte(int i) {
		return this.cartesEnMain.get(i);
	}
	/**
	 * Permet d'obtenir le nombre de cartes en main
	 * @return
	 */
	public int getNombreDeCarte() {
		return this.cartesEnMain.size();
	}


	/**
	 * Permet de donner une carte en l'enlevant de sa main
	 * @param indice de la carte à retirer
	 * @return la carte à retirer
	 */
	public Carte donnerCarte(int indice) {
		return this.cartesEnMain.remove(indice);
	}
	/**
	 * 
	 * @return
	 */
	public ArrayList<Carte> getJeuxEnMain() {
		return this.cartesEnMain;
	}

	public void setJeuxEnMain(ArrayList<Carte> carteEchange) {
		this.cartesEnMain = carteEchange;
	}

	public ArrayList<Carte> getCartesEnMain() {
		return cartesEnMain;
	}

	public ListIterator<Carte> getIterator() {
		return i;
	}

	public void creerIterator() {
		i = cartesEnMain.listIterator();
	}

	public int getindexcarte(Carte c) {
		return this.cartesEnMain.indexOf(c);
	}
}
