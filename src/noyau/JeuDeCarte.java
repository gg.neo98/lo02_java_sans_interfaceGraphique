package noyau;
/**
 * @version 2.0
 * @author guillaume
 *	le but de cette classe est de cr�er les cartes de cr�er la pioche et donner les cartes aux joueurs
 *
 */

import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;

import noyau.Controleur;

/**
 * Permet de créer un jeu de carte en fonction des paramètres de la partie
 * (nombre de cartes)
 * @see Controleur#Init(console.Affichable)
 *
 */
public class JeuDeCarte {

	protected LinkedList<Carte> tabCartes;
	private int nbCartes;

	public JeuDeCarte(int nombreDeCarte) {
		this.nbCartes = nombreDeCarte;
		this.tabCartes = new LinkedList<Carte>();
	}

	/**
	 * 
	 * Permet de créer les cartes avant de les stocker dans une linkedlist
	 * constituant le jeu
	 * 
	 * 
	 * @param le nombre de carte de type entier
	 *            
	 * 
	 */
	public void creerJeu(int nbCartes)

	{

		if (nbCartes == 32) {

			for (int i = 0; i < 4; i++) {
				for (int k = 0; k < 8; k++) {
					this.tabCartes.add(new Carte(i, k));
				}
			}
			
		} else if (nbCartes == 34) {
			for (int i = 0; i < 4; i++) {
				for (int k = 0; k < 8; k++) {
					this.tabCartes.add(new Carte(i, k));
				}
			}
			this.tabCartes.add(new Carte(4));
			this.tabCartes.add(new Carte(5));
			
		} else if (nbCartes == 52) {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 13; k++) {
					this.tabCartes.add(new Carte(j, k));
				}
			}
		
		} else if (nbCartes == 54) {
			for (int j = 0; j < 4; j++) {
				for (int k = 0; k < 13; k++) {
					this.tabCartes.add(new Carte(j, k));
				}
			}
			this.tabCartes.add(new Carte(4));
			this.tabCartes.add(new Carte(5));
			
		}
	}

	/**
	 * Permet de mélanger le jeu
	 * 
	 */
	public void melangerJeu() {
		Collections.shuffle(this.tabCartes);

	}

}


