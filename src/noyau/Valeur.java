package noyau;

/**
 * Enumération des différentes valeurs des cartes
 */
public enum Valeur {

	as("as"), roi("roi"), dame("dame"), valet("valet"), dix("dix"), neuf("neuf"), huit("huit"), sept("sept"), six(
			"six"), cinq("cinq"), quatre("quatre"), trois("trois"), deux("deux");

	private String valeur;

	Valeur(String valeur) {
		this.valeur = valeur;
	}


	public String getValeur() {
		return this.valeur;
	}
}
