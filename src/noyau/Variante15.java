package noyau;

import console.Affichable;
import console.Affichage;
/**
 *@see Variante1
 */
public class Variante15 extends Variante1
{
	
	public Variante15(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
  	public int sept(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{	
		int choix;
	
		//demander si vous voulez passer le tour
		do
		{
			choix =affichage.notifUtilisateuretDonnerInformation("voulez vous faire sauter le tour du jouer suivant 1: oui 0: non");
		}while(choix !=1||choix !=0);
		if (choix == 1)
			{return 2;}
		else 
		{
			return 2;
		}
	}
	
	@Override
  	public int huit(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		return 1;
		//problème du changement de couleur
	}
	
	@Override
  	public int dix(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche)throws PoserCarteException
  	{	
			if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
				{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
			else
			{
				if(joueur.getNombreDeCarte()==0)
				{
					pioche.piocher(1, joueur, pile);
					return 0;
				}
				else
				{
					return 0;
				}
			}
  	}
			
	@Override
	public int valet(Joueur joueur , Joueur suivant , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		
				{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
			else
				{
					if (carteJouer.getNomCouleur()=="pique")
						{pioche.piocher(4,suivant, pile);}
					return 1;
				}
  	}
	
	@Override
  	public int dame(Joueur joueur , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			int choix;
			//demander ce que l'on veut faire;
			if (joueur instanceof JoueurHumain )
			{
				do
				{	
					affichage.notifUtilisateur("voulez Vous changer le sens du jeu?");
					affichage.notifUtilisateur( "1 : non");
					choix=affichage.notifUtilisateuretDonnerInformation("2 : oui");
				}while (choix!=1&&choix !=2);
			}
			else
			{
				if ((1+(int)Math.random()*(5)%2)==0)
					{choix =1;}
				else 
					{choix =2;}
			}
			if (choix==1)
				{return 2;}
			else
				{Controleur.changerSensJeux();
				return 1;}
		}
	}
	
	@Override
	public  int as(Joueur joueur , Joueur suivant,Carte carteJouer , Pile pile , Pioche pioche)throws PoserCarteException
 	{
		if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())
			&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
 			{
				throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);
 			}
 		else
 			{	
 					Carte carte =null;
 					//tour
 					if (suivant instanceof JoueurHumain)
 					{
 							suivant.parcourirMain();
 							int numeroDeCarte = affichage.choixCarte(suivant);
 							if(numeroDeCarte>-1)
 							{
 								carte=suivant.getCarte(numeroDeCarte);
 							} 
 								
 					 }
 					else 
 					{
 						JoueurArtificiel IA1 = (JoueurArtificiel)suivant;
 						carte = IA1.strat.executer(IA1, pile, pioche);
 					}
 					if (carte!=null)
 					{
 						//suivant.jouerCarte(carte, pile);
 						if (carte.getNomValeur()=="huit")
 						{
 							suivant.jouerCarte(carte, pile);
 							return 2;
 						}
 						else if (carte.getNomValeur()=="as")
 						{
 							suivant.jouerCarte(carte, pile);
 							return 2;
 						}
 					}
 							pioche.piocher(2,suivant, pile);
 							return 1;
 				}
 	}
}
