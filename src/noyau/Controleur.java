package noyau;

import console.Affichage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;
import console.*;

/**
 * Classe pouvant se comparer à un maitre du jeu. Elle gère permet de gérer la
 * partie Aucun controleur n'est créé, ses methodes sont statiques
 */
public class Controleur {

	private static Regle regle;
	private static int sensJeu;

	private static LinkedList<Joueur> joueurs;

	private static Pile pile;
	private static Pioche pioche;
	private static boolean gagner;
	private static int nbJoueurs;
	private static int nbCartes;
	private static Scanner scanner = new Scanner(System.in);
	private static Affichable affichage;

	/**
	 * Methode d'initialisation de la partie (création des joueurs, distribution des
	 * cartes, choix de la variate)
	 * 
	 * @param un  objet de type affichable utilisé pour les interactions joueurs
	 *           
	 * @see Affichage#main(String[])
	 */
	public static void Init(Affichable a) {
		affichage = a;
		StrategieClassique strategie = new StrategieClassique();
		int s = affichage.menuPrincipal();
		sensJeu = 1;
		if (s == 1) {

			nbJoueurs = affichage.nombreJoueurs();
			joueurs = new LinkedList<Joueur>();
			Joueur j1 = new JoueurHumain();
			joueurs.add(0, j1);
			for (int i = 1; i < nbJoueurs; i++) {
				Joueur j = new JoueurArtificiel(strategie);
				joueurs.add(j);
			}

		}

		else if (s == 2) {
			System.exit(0);

		}
		nbCartes = affichage.tailleJeu();
		JeuDeCarte jeu = new JeuDeCarte(nbCartes);
		jeu.creerJeu(nbCartes);
		jeu.melangerJeu();
		distribuerCartes(jeu.tabCartes, 5);
		pioche = new Pioche(jeu.tabCartes, affichage);
		pile = new Pile(pioche);

		setRegle();
		partie();
	}

	/**
	 * Methode gèrant la partie
	 * Une boucle while est utilisée tant qu'aucun joueur n'a gagné
	 * Les joueurs jouent chacuns leur tour selon le sens du jeu
	 * 
	 */

	public static void partie() {
		gagner = false;
		int tour = 0;
		Joueur joueur;
		for (int i = 1; i < joueurs.size(); i++) {
			affichage.afficherNbCarteAdversaire(joueurs.get(i));
		}
		while (gagner == false) {
			affichage.afficherCarteJouee(pile.getDerniereCarteJouee());
			joueur = joueurs.get(tour);
			if (joueur instanceof JoueurHumain) {
				try {
					joueur.parcourirMain();
					int numeroDeCarte = affichage.choixCarte(joueur);
					if (numeroDeCarte == -1) {
						pioche.piocher(1, joueur, pile);
						tour = tour + Controleur.getSensJeu() * 1;
					} else {
						Carte carteJouee = joueur.getCarte(numeroDeCarte);
						tour = tour + regle.applicationRegle(joueur, getJoueurSuivant(joueur), carteJouee, pile, pioche)
								* Controleur.getSensJeu();
						joueur.jouerCarte(numeroDeCarte, pile);
					}
				} catch (PoserCarteException e) {
					affichage.notifUtilisateur("vous ne pouvez pas jouer cette carte");
				}
			} else if (joueur instanceof JoueurArtificiel) {

				Carte carteJouee = ((JoueurArtificiel) joueur).strat.executer(joueur, pile, pioche);

				if (carteJouee == null) {
					pioche.piocher(1, joueur, pile);
					tour = tour + Controleur.getSensJeu() * 1;
				} else {
					try {
						tour = tour + Controleur.getSensJeu()
								* regle.applicationRegle(joueur, getJoueurSuivant(joueur), carteJouee, pile, pioche);
						joueur.jouerCarte(carteJouee, pile);
					} catch (PoserCarteException e) {
					}
				}
				affichage.afficherNbCarteAdversaire(joueur);
			}
			if (joueur.getCartesEnMain().size() == 0) {
				gagner = true;
				affichage.notifUtilisateur("le joueur numero : " + (tour - 1) + "a gagner");
			}
			if (tour >= 0) {
				tour = tour % nbJoueurs;
			} else {
				tour = nbJoueurs + ((tour % nbJoueurs));
			}
		}
	}
/**
 * Permet de distribuer les cartes 
 * @param la paquet de carte à distribuer sous forme de linkedlist de cartes
 * @param le nombre de carte constituant une main
 */
	public static void distribuerCartes(LinkedList<Carte> paquet, int nbCartesMain) {
		int c = paquet.size();
		for (int i = 0; i < nbCartesMain; i++) {
			for (int k = 0; k < getNbJoueurs(); k++) {
				joueurs.get(k).prendreCarte(paquet.get(c - 1));
				paquet.remove(c - 1);
				c--;
			}
		}
	}

	/**
	 * Permet au joueur de choisir la variante pour la partie en cours
	 * Un Objet du type de la variante en question est créé
	 * La stratégie aggressive est appliquée aux joueur artificiel si celle ci est activée et que la variante le permet
	 */

	public static void setRegle() {
		ListIterator<Joueur> iter = joueurs.listIterator();
		int s = affichage.stratAgro();
		int choixRegle;
		do {
			choixRegle = affichage.choixVariante();
			switch (choixRegle) {
			case 1:
				regle = new Variante1(affichage);
				break;
			case 2:
				regle = new variante2(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 3:
				regle = new variante3(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 4:
				regle = new Variante4(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 5:
				regle = new Variante5(affichage);
				break;
			case 6:
				regle = new Variante6(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 7:
				regle = new Variante7(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 8:
				regle = new Variante8(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 9:
				regle = new Variante9(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 10:
				regle = new Variante10(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 11:
				regle = new Variante11(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 12:
				regle = new Variante12(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 13:
				regle = new Variante13(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 14:
				regle = new Variante14(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 15:
				regle = new Variante15(affichage);
				while (iter.hasNext() && s == 1) {
					Joueur j =iter.next();
					if(j instanceof JoueurArtificiel) {
						((JoueurArtificiel)j).setStrat(new StrategieAggressive());
					}
				}
				break;
			case 16:
				regle = new Variante16(affichage);
				break;
			case 17:
				regle = new Variante17(affichage);
				break;
			default:
				affichage.notifUtilisateur("votre choix n'est pas disponible.");
				break;
			}
		} while ((choixRegle < 1) && (choixRegle > 17));
	}

	/**
	 * Getter du nombre de joueurs
	 * @return nbJoueurs
	 */
	public static int getNbJoueurs() {
		return nbJoueurs;
	}
	/**
	 * Setter du nombre de joueurs
	 * @param nombreJoueurs
	 */
	public static void setNbJoueurs(int nombreJoueurs) {
		nbJoueurs = nombreJoueurs;
	}
	/**
	 * Permet de chnger le sens du jeu
	 */
	public static void changerSensJeux() {
		sensJeu = sensJeu * (-1);
	}
	/**
	 * Permet d'obtenir le joueur précédent du joueur passé en paramètre
	 * @param joueur
	 * @return joueur précédent
	 */
	public static Joueur getJoueurPrecedent(Joueur joueur) {
		int indice = joueurs.indexOf(joueur);
		if (indice == 0) {
			return joueurs.get(joueurs.size());
		} else {
			return joueurs.get(indice - 1);
		}
	}
	/**
	 * Permet d'obtenir le joueur suivant le joueur passé en paramètre
	 * @param joueur
	 * @return joueur suivant
	 */
	public static Joueur getJoueurSuivant(Joueur joueur) {
		int indice = joueurs.indexOf(joueur);
		if (indice == nbJoueurs - 1) {
			return getJoueur(0);
		} else {
			return joueurs.get(indice + 1);
		}
	}
	/**
	 * Getter du sens du jeu
	 * @return sensJeu
	 */
	public static int getSensJeu() {
		return sensJeu;
	}
	/**
	 * Getter du joueur
	 * @param l'indice du joueur dans le tableau des joueurs
	 * @return
	 */
	public static Joueur getJoueur(int indice) {
		return joueurs.get(indice);
	}
	/**
	 * Permet de passer la partie à l'état de "gagnée"
	 */
	public static void gagner() {
		gagner = true;
	}

	public static int posJoueur(Joueur j) {
		return joueurs.indexOf(j);
	}
	/**
	 * Getter de la variante utilisée pour la partie en cours
	 * @return la variante
	 */
	public static Regle getRegle() {
		return regle;
	}
}
