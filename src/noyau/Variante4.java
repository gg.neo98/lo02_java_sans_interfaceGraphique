package noyau;

import console.Affichable;
/**
 *@see Variante1
 */
public class Variante4 extends Variante1
{
	public Variante4(Affichable a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int huit(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
		{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
	else
		{return 2;}
	}
	
	@Override
	public int sept(Joueur joueur , Joueur suivant  ,Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	if ((pile.getDerniereCarteJouee().getNomValeur()!=carteJouer.getNomValeur())&&(pile.getDerniereCarteJouee().getNomCouleur())!=carteJouer.getNomCouleur())
			{throw new PoserCarteException(joueur, carteJouer, pile.getDerniereCarteJouee(), affichage);}
		else
		{
			pioche.piocher(2,suivant, null);
			return 1;
		}
	}
	
	@Override
	public int valet(Joueur joueur ,Joueur suivant  , Carte carteJouer , Pile pile ,Pioche pioche) throws PoserCarteException
	{ 	
		return 1;
	}
	
}
