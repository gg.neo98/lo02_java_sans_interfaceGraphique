package noyau;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;
/**
 * 
 * Classe définissant la stratégie classique des joueurs artificiels, à savoir jouer la première carte que le joueur peut jouer
 *
 */
public class StrategieClassique implements Strategie {
	/**
	 * Application de la stratégie au joueur passé en paramètre
	 * @param joueur
	 * @param pile
	 * @param pioche
	 */
	public Carte executer(Joueur j, Pile pile, Pioche pioche) {

		Carte c = pile.getDerniereCarteJouee();

		ArrayList<Carte> main = j.getCartesEnMain();
		ListIterator<Carte> i = main.listIterator();
		while (i.hasNext()) {
			Carte c2 = i.next();
			if (c2.equals(c)) {
				// j.jouerCarte(i.nextIndex(), pile);
				return c2;
			}
		}

		// pioche.piocher(1, j, pile);
		return null;
	}

}
