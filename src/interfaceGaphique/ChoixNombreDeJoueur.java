package interfaceGaphique;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 * la classe permet de demander le nombre de carte
 * @author guillaume
 *
 */
public class ChoixNombreDeJoueur
{
	
	private int nbJoueur;
	/**
	 * le contructeur prend en param�tre l'affichage et affiche un menu deroulant permettant d'obtenir le choix
	 * @param c
	 */
	public ChoixNombreDeJoueur(JFrame c)
	{
		String [] nbJoueur= {"2","3","4"};
		 JOptionPane jop = new JOptionPane();

		    String nom = (String)jop.showInputDialog(null,"entrer le nombre de joueur que vous souhaiter"  , 
		    		"nombre de joueur",JOptionPane.QUESTION_MESSAGE,null,nbJoueur, nbJoueur[0]);
		int i =0;
		while (nom!=nbJoueur[i])
		{i++;}
		this.nbJoueur=i+2;
	}
	
	public int nbjoueur()
	{return this.nbJoueur;}
}
