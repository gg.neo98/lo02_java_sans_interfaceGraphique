package interfaceGaphique;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 * la Classe permet de choisir la strat�gie que l'ordinateur doit adopter au cour de �a parti
 * @author guillaume
 *
 */
public class ChoixStrat extends JFrame implements ActionListener {
	private int choixStart;

	/**
	 * le contructeur prend en param�tre l'affichage a utilis�
	 * @param c
	 */
	public ChoixStrat(JFrame c)
	{
		String []variante= {"strat�gie classique","strat�gie agressive"};
		 JOptionPane jop = new JOptionPane();

		    String nom = (String)jop.showInputDialog(null,"entrer la variante que vous souhaiter"  , 
		    		"choix variante",JOptionPane.QUESTION_MESSAGE,null,variante, variante[0]);
		int i =0;
		while (nom!=variante[i])
		{i++;}
		i++;
		this.choixStart=i;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * la methode retourne le choix de r�gle effectuer
	 * @return
	 */
	public int getchoix()
	{
		return this.choixStart;
	}
}
