package interfaceGaphique;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JPanel;
import java.awt.Color;
/**
 * la classe permet d'effecuter la vue de l'affichage graphique
 * @author guillaume
 *
 */
public class InterfaceGraphique {

	private JFrame frame;
	private JPanel panel;
	private JTextPane textPane;
	private JTextPane textPane1;
	private JTextPane textPane2;
	private JTextPane textPaneCentre;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					InterfaceGraphique window = new InterfaceGraphique();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public InterfaceGraphique() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(0, 128, 0));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		 textPane = new JTextPane();
		textPane.setBackground(new Color(0, 128, 0));
		frame.getContentPane().add(textPane, BorderLayout.NORTH);
		
		this.textPane1 = new JTextPane();
		this.textPane1.setBackground(new Color(0, 128, 0));
		frame.getContentPane().add(this.textPane1, BorderLayout.WEST);
		
		this.textPane2 = new JTextPane();
		this.textPane2.setBackground(new Color(0, 128, 0));
		frame.getContentPane().add(this.textPane2, BorderLayout.EAST);
		
		this.textPaneCentre  = new JTextPane();
		this.textPaneCentre.setBackground(new Color(0, 128, 0));
		frame.getContentPane().add(this.textPaneCentre, BorderLayout.CENTER);
		
		this.panel = new JPanel();
		panel.setBackground(new Color(0, 100, 0));
		frame.getContentPane().add(panel, BorderLayout.SOUTH);
	}
	public JFrame getWindows()
	{
		return this.frame;
	}
	
	public JTextPane gettextPane()
	{
		return this.textPane;
	}
	
	public JTextPane gettextPane1()
	{
		return this.textPane1;
	}
	
	public JTextPane gettextPane2()
	{
		return this.textPane2;
	}
	
	public JTextPane gettextPaneCenter()
	{
		return this.textPaneCentre;
	}
	public JPanel getJpanel()
	{ return this.panel;}
}
