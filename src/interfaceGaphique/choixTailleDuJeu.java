package interfaceGaphique;

import javax.swing.JOptionPane;
/**
 * la Classe permet de choisir le nombre de carte dans un menu d�roulant
 * @author guillaume
 *
 */
public class choixTailleDuJeu
{
	private int taille;
	/**
	 * la methode permet d'afficher une boite de dialoque avec menu d�roulant permetant de retourner le choix voulu
	 */
	public choixTailleDuJeu()
	{
		String [] tailleDuJeu= {"32","34","52","54"};
		 JOptionPane jop = new JOptionPane();

		    String nom = (String)jop.showInputDialog(null,"entrer la de jeux  que vous souhaiter"  , 
		    		"taille du jeu",JOptionPane.QUESTION_MESSAGE,null,tailleDuJeu, tailleDuJeu[0]);
		int i =0;
		while (nom!=tailleDuJeu[i])
		{i++;}
		switch (i)
		{
			case 0: this.taille=32;break;
			case 1: this.taille=34;break;
			case 2: this.taille=52;break;
			case 3: this.taille=54;break;
		}
	}
/**
 * la methode retourne la valeur souhait�e
 * @return
 */
	public int getTaille()
		{return this.taille;}
}