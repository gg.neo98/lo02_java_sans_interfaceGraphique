package interfaceGaphique;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import console.*;
import noyau.Carte;
import noyau.Controleur;
import noyau.Joueur;
/**
 * la classe permet de controler l'affichage grahique en implementant affichable
 * @author guillaume
 *
 */
public class ControleGraphique implements Affichable,ActionListener
{
	private InterfaceGraphique c;
	private int carteJouer;
	private LinkedList<JButton> carte;
	private boolean ajouer;
	
	public ControleGraphique() 
	{
		this.c=new InterfaceGraphique();
		c.getWindows().setVisible(true);
	}

	@Override
	public int menuPrincipal() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public int nombreJoueurs() 
	{
		ChoixNombreDeJoueur d =new  ChoixNombreDeJoueur(this.c.getWindows());
		return d.nbjoueur();
	}

	@Override
	public int tailleJeu() 
	{
		choixTailleDuJeu t =new choixTailleDuJeu();
		return t.getTaille() ;
	}

	@Override
	public int choixVariante() {
		choixRegle r= new choixRegle(this.c.getWindows());
		return r.regle();
	}

	@Override
	public int choixCarte(Joueur j) 
	{
		this.ajouer =false;
		this.carte = new LinkedList<JButton>();
		this.carte.add(0, new JButton("piocher"));
		c.getJpanel().validate();
		this.carte.get(0).addActionListener(this);
		for (int i =1;i<=j.getNombreDeCarte();i++)
		{
			this.carte.add( new JButton(j.getCarte(i-1).toString()));
			c.getJpanel().add(this.carte.get(i));
			c.getJpanel().validate();
			this.carte.get(i).addActionListener(this);
		}
		return this.carteJouer;
	}

	@Override
	public void notifUtilisateur(String msg) 
	{
		JOptionPane.showMessageDialog(c.getWindows(),msg,null,JOptionPane.PLAIN_MESSAGE);
		
	}

	@Override
	public int notifUtilisateuretDonnerInformation(String msg) {
		String txt=JOptionPane.showInputDialog(c.getWindows(),msg,null,JOptionPane.PLAIN_MESSAGE);
		int i = Integer.parseInt(txt); 
		return i ;
	}

	@Override
	public int choixCouleur() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void direCarte(Joueur j) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void direContreCarte(Joueur j) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afficherCarteJouee(Carte c) {
		this.c.gettextPaneCenter().setText(c.toString());
		
	}

	@Override
	public void afficherNbCarteAdversaire(Joueur j) {
		
		int pos=Controleur.posJoueur(j);
		switch (pos)
		{	
			case 1 : this.c.gettextPane().setText("ce joueur a "+j.getNombreDeCarte()+"carte");
			case 2 : this.c.gettextPane1().setText("ce joueur a "+j.getNombreDeCarte()+"carte");
			case 3 : this.c.gettextPane2().setText("ce joueur a "+j.getNombreDeCarte()+"carte");
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object source =e.getSource();
		this.carteJouer=-1;
		int i=0;
		while ((this.carte.get(carteJouer+1)!=source)&&i<this.carte.size())
		{
			i++;
		}
		if ((this.carte.get(carteJouer+1)!=source)&&i<this.carte.size())
		{this.carteJouer=i;}
		this.ajouer=true;
		
	}
	
	public static void main(String []args)
	{
		ControleGraphique affichage =new ControleGraphique();
		Controleur.Init(affichage);
		
	}

	@Override
	public int stratAgro() {
		ChoixStrat st= new ChoixStrat(this.c.getWindows());
		// TODO Auto-generated method stub
		return st.getchoix();
	}
	
}
