package interfaceGaphique;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
/**
 * la classe permet d'afficher un menu d�roulant permetttant d'afficher les differentes r�gles disponibles
 * @author guillaume
 *
 */
public class choixRegle extends JFrame implements ActionListener
{
	 private int regle;
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * le contructeur permet d'afficher le menu d�roulant et de modifier la valeur de r�gle qui repr�sant le choix effectue
	 * @param c l'affichage
	 */
	public choixRegle(JFrame c)
	{
		String []variante= {"variante1","variante2","variante3","variante4","variante5","variante6","variante7","variante8","variante9"};
		 JOptionPane jop = new JOptionPane();

		    String nom = (String)jop.showInputDialog(null,"entrer le numero de r�gle que vous souhaiter"  , 
		    		"choix r�gle",JOptionPane.QUESTION_MESSAGE,null,variante, variante[0]);
		int i =0;
		while (nom!=variante[i])
		{i++;}
		i++;
		this.regle=i;
	}
	/**
	 * retourne le choix de r�gle souhait�
	 * @return
	 */
	public int regle()
	{return this.regle;}
}
