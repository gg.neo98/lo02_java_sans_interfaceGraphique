package console;

import java.util.Scanner;

import noyau.Carte;
import noyau.Controleur;
import noyau.Joueur;

/**
 * Classe implémentant l'interface affichable Permet de gérer le contenu
 * affichable et les interactions joueur
 * 
 *
 */

public class Affichage implements Affichable { // deviendra potentiellement une classe abstraite
	private Scanner scanner;

	/**
	 * Constructeur de la classe Création d'un scanner pour récupérrer les entrées
	 * clavier
	 * 
	 */
	public Affichage() {
		// TODO Auto-generated constructor stub
		this.scanner = new Scanner(System.in);
	}

	/**
	 * Permet d'afficher le menu principal et de lancer une partie
	 * 
	 * @return le choix du joueur pour le lancement de la partie
	 */
	public int menuPrincipal() {
		System.out.println("## 8 Américain ##");
		System.out.println("\nMenu :\n");
		System.out.println("\n (1) Lancer partie\n");
		System.out.println("\n (2) Quitter\n");
		int s = scanner.nextInt();
		if (s < 1 || s > 2) {
			this.menuPrincipal();
		}
		return s;
	}

	/**
	 * Permet au joueur d'initialiser le nombre de joueur de la partie
	 * 
	 * @return le nombre de joueurs
	 */
	public int nombreJoueurs() {
		System.out.print("Nombre de joueurs (entre 1 et 4:\t");
		int s = scanner.nextInt();
		if (s < 1 || s > 4) {
			this.nombreJoueurs();
		}
		return s;
	}

	/**
	 * Permet au joueur de choisir la taille du paquet
	 * 
	 * @return la taille du paquet
	 */
	public int tailleJeu() {
		System.out.print("Taille du paquet (32/34/52/54):\t");
		int s = scanner.nextInt();
		if (s != 32 && s != 34 && s != 52 && s != 54) {
			this.tailleJeu();
		}
		return s;
	}

	/**
	 * Permet au joueur de choisir la variante
	 * 
	 * @return l'indice de la variante
	 */
	public int choixVariante() {
		System.out.println("Choix de la variante :\t");
		System.out.println("Variante 1 (1)\t");
		System.out.println("Variante 2 (2)\t");
		System.out.println("Variante 3 (3)\t");
		System.out.println("Variante 4 (4)\t");
		System.out.println("Variante 5 (5)\t");
		System.out.println("Variante 6 (6)\t");
		System.out.println("Variante 7 (7)\t");
		System.out.println("Variante 8 (8)\t");
		System.out.println("Variante 9 (9)\t");
		System.out.println("Variante 10 (10)\t");
		System.out.println("Variante 11 (11)\t");
		System.out.println("Variante 12 (12)\t");
		System.out.println("Variante 13 (13)\t");
		System.out.println("Variante 14 (14)\t");
		System.out.println("Variante 15 (15)\t");
		System.out.println("Variante 16 (16)\t");
		System.out.println("Variante 17 (17)\t");

		int s = scanner.nextInt();
		if (s < 1 || s > 17) {
			System.out.println("Choix Invalide\t");
			this.choixVariante();
		}
		return s;
	}

	/**
	 * Permet au joueur d'activer la stratégie aggressive
	 * 
	 * @return le choix du joueur
	 */
	public int stratAgro() {
		System.out.println("activer la stratégie aggressive ?\n");
		System.out.println("(1 = oui; 0 = non)\n");
		int s = scanner.nextInt();
		if (s != 0 && s != 1) {

			System.out.println("Entrée invalide\n");
			this.stratAgro();
		}
		return s;
	}

	/**
	 * Permet au joueur de sélectionner la carte qu'il veut jouer
	 * 
	 * @param le joueur
	 *   
	 * @return la carte selectionnée
	 */
	public int choixCarte(Joueur j) {
		System.out.println("Quelle carte jouer ?");
		System.out.println("entrer -1 si vous ne pouver rien jouer");
		int s = scanner.nextInt();
		if (s < -1 || s >= j.getCartesEnMain().size()) {
			System.out.println("Carte invalide");
			s = this.choixCarte(j);
		}
		return s;
	}

	/**
	 * Permet d'afficher un message au joueur
	 * 
	 * @param le  message
	 *           
	 */
	public void notifUtilisateur(String msg) {
		System.out.println(msg);
	}

	/**
	 * Permet de notifier le joueur et de récupérer l'entrée clavier suivante
	 * 
	 * @return l'entrée clavier sous forme d'entier
	 */
	public int notifUtilisateuretDonnerInformation(String msg) {
		System.out.println(msg);
		return scanner.nextInt();
	}

	/**
	 * Permet au joueur de choisir une couleur
	 * 
	 * @return l'indice de la couleur
	 */
	public int choixCouleur() {
		int s;
		do {
			System.out.println("1: trefle");
			System.out.println("2: coueur");
			System.out.println("3: carreau");
			System.out.println("4: pique");
			s = scanner.nextInt();
		} while (s < 1 || s > 4);
		return s;
	}

	/**
	 * Permet au joueur de dire carte
	 * 
	 * @param le  joueur
	 *           
	 */
	public void direCarte(Joueur j) {
		System.out.println( ": Carte !!");

	}

	/**
	 * Permet au joueur de dire contrecarte
	 * 
	 * @param le  joueur
	 *           
	 */
	public void direContreCarte(Joueur j) {
		System.out.println( "Contre carte !!");
	}

	/**
	 * Permet d'afficher la dernière carte jouée
	 * 
	 * @param la dernière carte jouée
	 */
	           
	public void afficherCarteJouee(Carte c) {
		System.out.println("la derniere carte jou�e" + c.toString());
	}
	 /**
	  * Point de lancement du programme
	  * @param args
	  */
	public static void main(String[] args) {

		Affichage console = new Affichage();
		Controleur.Init(console);


	}
	
	/**
	 * Permet d'afficher le nombre de carte d'un adversaire
	 * @param l'adversaire
	 */
	@Override
	public void afficherNbCarteAdversaire(Joueur j) {
		System.out.println("le joueur a " + j.getNombreDeCarte() + "carte");
	}

}
