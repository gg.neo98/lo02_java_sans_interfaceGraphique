package console;

/** 
 * Interface gérant le contenu affichable et les interactions avec le joueur
 * @see Affichable
 * 
 */
import noyau.Carte;
import noyau.Joueur;

public interface Affichable {
/**
 * le menuPrincipale doit afficher si on veut jouer ou pas il faut retourner 1 si on veut jouer et 2 sinon attention
 * il faut que la v�rification de l'entier soit faite  dans celle-ci 
 * @return
 */
	public int menuPrincipal();
/**
 * methode servant � retourner le nombre de joueur qu'il y aura aucours de la parti. Pour ce faire,il faut que la m�thode v�rifie que la valeur retourn�e est positive 
 * @return
 */
	public int nombreJoueurs();
/**
 * la methode retourne le nombre de carte du paquet soit 32-34-52-54 il faut que celle-ci v�rifie la validit� de l'entr�
 * @return
 */
	public int tailleJeu();
/**
 * la m�thode retourne le choix de la variante sachant que cell-ci doit �tre compris entre 1-17 la methode doit faire la v�rification
 * 
 */
	public int choixVariante();
/**
 * la methode affiche le carte du joueur et renvoie ensuite la saisie effecuer
 * @param j
 * @return
 */
	public int choixCarte(Joueur j);

	/**
	 * la methode affiche un message a l'utilisateur. Celui-ci est pass� en param�tre
	 * @param msg
	 */
	public void notifUtilisateur(String msg);

	/**
	 *  la methode affiche un message a l'utilisateur. Celui-ci est pass� en param�tre et renvoie un entier en fonction de la saisie attention la methode ne v�rifie rien
	 *  
	 * @param msg
	 * @return
	 */
	public int notifUtilisateuretDonnerInformation(String msg);
/**
 * la methode permet de retourner un entier correspondant a la couleur 
 * @return
 */
	public int choixCouleur();
/**
 * la methode permet de dire carte;
 * @param j
 */
	public void direCarte(Joueur j);
/** 
 * la methode permet de dire contre carte
 * @param j
 */
	public void direContreCarte(Joueur j);

/**
 * la methode permet d'afficher la carte jou�e elle prend en paramt�re celle-ci
 * @param c
 */
	public void afficherCarteJouee(Carte c);
/**
 * la methode permet d'afficher le nombre de carte de l'adversaire
 * @param j
 */
	public void afficherNbCarteAdversaire(Joueur j);
/**
 * la methode permet de choisir la strat�gie a effectuer
 * 
 * @return
 */
	public int stratAgro();
}
